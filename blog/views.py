from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic import CreateView,UpdateView, DeleteView
from django.http import HttpResponse
from .models import Posts


def home(request):
	context = {
		'posts':Posts.objects.all()
	}
	return render(request, 'blog/home.html',context)

class PostListView(ListView):
	model = Posts
	template_name = 'blog/home.html'
	context_object_name = 'posts'
	ordering = ['-date_posted']
	paginate_by = 2

class PostDetailView(DetailView):
	model = Posts
	
class PostCreateView(LoginRequiredMixin, CreateView):
	model = Posts
	fields = ['title','content']

	def form_valid(self, form):
		form.instance.author = self.request.user
		return super().form_valid(form)
	# def get_queryset(self):
	# 	return Posts.objects.filter(title='Blog 2')

class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
	model = Posts
	fields = ['title','content']

	def form_valid(self, form):
		form.instance.author = self.request.user
		return super().form_valid(form)

	def test_func(self):
		post = self.get_object()
		if self.request.user == post.author:
			return True
		return False

class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
	model = Posts
	success_url = '/'

	def test_func(self):
		post = self.get_object()
		if self.request.user == post.author:
			return True
		return False

def about(request):
	return render(request,'blog/about.html',{'title':'About'})